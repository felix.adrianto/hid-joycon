obj-m += hid-joycon.o
 
all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
 
clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean

install: hid-joycon.ko
	install -d /lib/modules/$(shell uname -r)/kernel/drivers/hid
	install -m 644 hid-joycon.ko /lib/modules/$(shell uname -r)/kernel/drivers/hid
	depmod -a

uninstall:
	rm /lib/modules/$(shell uname -r)/kernel/drivers/hid/hid-joycon.ko
